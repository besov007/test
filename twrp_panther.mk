#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base.mk)

#other
#$(call inherit-product, $(SRC_TARGET_DIR)/product/virtual_ab_ota.mk)
#$(call inherit-product, $(SRC_TARGET_DIR)/product/languages_full.mk)

# other
# Installs gsi keys into ramdisk, to boot a developer GSI with verified boot.
$(call inherit-product, $(SRC_TARGET_DIR)/product/gsi_keys.mk)

# Inherit from panther device
$(call inherit-product, device/google/panther/device.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/twrp/config/common.mk)

PRODUCT_DEVICE := panther
PRODUCT_NAME := twrp_panther
PRODUCT_BRAND := Android
PRODUCT_MODEL := Pixel 7
PRODUCT_MANUFACTURER := google

#other
PRODUCT_GMS_CLIENTID_BASE := android-google

