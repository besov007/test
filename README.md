Test device tree

1.First update \
panther: Initial TWRP device tree of original content from [Twrpdtgen](https://github.com/twrpdtgen/twrpdtgen#twrpdtgen)
```
.
├── [ 834]  Android.mk
├── [ 771]  AndroidProducts.mk
├── [2.8K]  BoardConfig.mk
├── [1.4K]  device.mk
├── [1.5K]  omni_panther.mk
├── [4.0K]  prebuilt
│   └── [   0]  dtb.img
├── [   0]  README.md
├── [4.0K]  recovery
│   └── [4.0K]  root
│       ├── [ 239]  android.hardware.health-service.gs201_recovery.rc
│       ├── [ 432]  init.recovery.gs201.rc
│       ├── [  46]  init.recovery.panther.rc
│       └── [ 124]  servicemanager.recovery.rc
├── [2.4K]  recovery.fstab
└── [ 786]  vendorsetup.sh

3 directories, 13 files
```

2.omni_panther.mk file rename twrp_panther.mk \
3.Update recovery.fstab and twrp.flags files \
4.Update kernal and dtb files
```
.
├── [ 834]  Android.mk
├── [ 771]  AndroidProducts.mk
├── [2.8K]  BoardConfig.mk
├── [1.4K]  device.mk
├── [4.0K]  prebuilt
│   ├── [566K]  dtb.img
│   └── [ 24M]  kernel
├── [ 904]  README.md
├── [4.0K]  recovery
│   └── [4.0K]  root
│       ├── [ 239]  android.hardware.health-service.gs201_recovery.rc
│       ├── [ 432]  init.recovery.gs201.rc
│       ├── [  46]  init.recovery.panther.rc
│       ├── [ 124]  servicemanager.recovery.rc
│       └── [4.0K]  system
│           └── [4.0K]  etc
│               ├── [4.1K]  recovery.fstab
│               ├── [2.6K]  twrp.flags
│               └── [4.0K]  vintf
│                   └── [4.0K]  manifest
│                       └── [ 201]  android.hardware.health-service.gs201.xml
├── [1006]  twrp_panther.mk
└── [ 786]  vendorsetup.sh

7 directories, 16 files
```

5.Update Android.mk and AndroidProducts.mk files
